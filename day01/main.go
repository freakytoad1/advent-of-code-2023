package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func main() {
	defer exeTime("main")()

	content, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))
	log.Printf("Part 1: %d", p1)
	log.Printf("Part 2: %d", p2)
}

func parse(input string) (int, int) {
	p1 := 0
	p2 := 0
	p1Reg := regexp.MustCompile(`(\d)`)
	p2Reg := regexp.MustCompile(`(one|two|three|four|five|six|seven|eight|nine|zero|\d)`)

	// iterate over every line, grab the value
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			continue
		}

		p1 += extractCalibrationValue(line, p1Reg)
		p2 += extractCalibrationValue(line, p2Reg)
	}

	return p1, p2
}

func extractCalibrationValue(input string, re *regexp.Regexp) int {
	var first, second string

	finds := re.FindAllString(input, -1)

	// first will always be the same, grab the digit
	first = spelledToDigit(finds[0])

	if len(finds) == 1 {
		// same first and last
		second = first
	} else {
		// diff last
		second = spelledToDigit(finds[len(finds)-1])
	}

	total, err := strconv.Atoi(fmt.Sprintf("%s%s", first, second))
	if err != nil {
		log.Printf("invalid #s [%s,%s] found for input [%s]: %v", first, second, input, err)
		return 0
	}

	return total
}

var spelledDigit = map[string]string{
	"one":   "1",
	"two":   "2",
	"three": "3",
	"four":  "4",
	"five":  "5",
	"six":   "6",
	"seven": "7",
	"eight": "8",
	"nine":  "9",
}

// spelledToDigit looks up a spelled # and returns the digit.
// otherwise the input is returned
func spelledToDigit(input string) string {
	if digit, found := spelledDigit[input]; found {
		return digit
	}
	return input
}

// func to calculate and print execution time
func exeTime(name string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s execution time: %v\n", name, time.Since(start))
	}
}
