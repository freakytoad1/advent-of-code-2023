package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	defer exeTime("main")()

	content, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatalf("unable to open input file: %v", err)
	}

	p1, p2 := parse(string(content))
	log.Printf("Part 1: %d", p1)
	log.Printf("Part 2: %d", p2)
}

func parse(input string) (int, int) {
	maxRed := 12
	maxGreen := 13
	maxBlue := 14

	p1 := 0
	p2 := 0

	// iterate over every line, grab the value
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			continue
		}

		// Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
		// -> [Game 1], [ 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green]
		gameSplit := strings.Split(line, ":")

		// grab the game ID
		// EX) Game 1 -> 1
		gameID := extractGameID(gameSplit[0])

		//  3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
		// -> [3 blue, 4 red] [1 red, 2 green, 6 blue] [2 green]
		gameSets := separateGameSets(strings.TrimSpace(gameSplit[1]))

		impossibleGame := false
		largestRed := 0
		largestGreen := 0
		largestBlue := 0

		for _, set := range gameSets {
			// check each color
			// mark impossible game for p1 if above max
			// save the largest color for the whole game
			currentRed := extractColorAmount(set, "red")
			if currentRed > maxRed {
				impossibleGame = true
			}
			if currentRed > largestRed {
				largestRed = currentRed
			}

			currentGreen := extractColorAmount(set, "green")
			if currentGreen > maxGreen {
				impossibleGame = true
			}
			if currentGreen > largestGreen {
				largestGreen = currentGreen
			}

			currentBlue := extractColorAmount(set, "blue")
			if currentBlue > maxBlue {
				impossibleGame = true
			}
			if currentBlue > largestBlue {
				largestBlue = currentBlue
			}
		}

		if !impossibleGame {
			p1 += gameID
		}

		// multiply each largest color together to get the power for game
		p2 += (largestRed * largestGreen * largestBlue)
	}

	return p1, p2
}

func extractGameID(input string) int {
	splitInput := strings.Split(input, " ")
	if len(splitInput) != 2 {
		log.Printf("invalid game id, input [%s], split up [%v]", input, splitInput)
		return 0
	}

	gameID, err := strconv.Atoi(splitInput[1])
	if err != nil {
		log.Printf("unable to parse game id out of [%v]: %v", splitInput, err)
		return 0
	}

	return gameID
}

// separates each full game into the individual shown sets
// each set {1 red, 2 blue, 3 green} is an element of the return slice
func separateGameSets(input string) []string {
	sets := []string{}
	splitInput := strings.Split(input, ";")

	for _, set := range splitInput {
		sets = append(sets, strings.TrimSpace(set))
	}

	return sets
}

// [1 red, 2 blue, 3 green], want red
// return 1
func extractColorAmount(input, color string) int {
	inputSplit := strings.Split(input, ",")

	for _, combo := range inputSplit {
		splitNumColor := strings.Split(strings.TrimSpace(combo), " ")

		if color == splitNumColor[1] {
			amount, err := strconv.Atoi(splitNumColor[0])
			if err != nil {
				log.Printf("unable to extract # of color [%s]: %v", combo, err)
				return 0
			}

			return amount
		}
	}

	return 0
}

// func to calculate and print execution time
func exeTime(name string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s execution time: %v\n", name, time.Since(start))
	}
}
